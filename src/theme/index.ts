import colors from './colors'
import cssColors from './cssColors'
import mediaScreens from './mediaScreens'

export default {
  colors,
  cssColors,
  mediaScreens,
}
